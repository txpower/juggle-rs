// TODO: put this somewhere else
// #[if(option_replace)]

pub fn drop<Any>(_:Any) {}

pub trait Replace {
    fn replace(&mut self,other:Self) -> Self;
}


// Builtin method for Option<T> with Rust 1.31.0-stable
// this is for backwards compatibility
impl<T> Replace for Option<T>{
    fn replace(&mut self,other:Option<T>) -> Option<T>{
        let out = self.take();
        (*self) = other;
        out
    }
}

#[cfg(test)]
#[test]
fn test_replace_option() {
    let mut x = None;
    assert_eq!(x.replace(1337),None);
    assert_eq!(x,Some(1337));
    let mut x = Some(42);
    assert_eq!(x.replace(23),Some(42));
    assert_eq!(x,Some(23));
}

// filter Vec : element != None
pub fn remove_none_from_vec<T>(l:&mut Vec<Option<T>>,none_hint:Option<usize>) -> Result<usize,()>{
    let len_before = l.len();
    let mut none_found:usize = 0;
    if let Some(n) = none_hint {
        if n > len_before {
            return Err(());
        }
    }
    let mut v=Vec::with_capacity(len_before-none_hint.unwrap_or(0));
    while let Some(x) = l.pop() {
        if x.is_some() {
            v.push(x)
        } else {
            none_found += 1;
        }
    }
    while let Some(x) = v.pop() {
        l.push(x)
    }
    Ok(none_found)
}

#[cfg(test)]
#[test]
fn test_remove_none_from_vec() {
    let mut x = vec![None,Some(1),Some(2),None,Some(4),Some(5),None,None,None];
    assert_eq!(remove_none_from_vec(&mut x,Some(5)).unwrap(),5);
    assert_eq!(x.len(),4);
    assert_eq!(remove_none_from_vec(&mut x,Some(0)).unwrap(),0);
    assert_eq!(x.len(),4);
}
