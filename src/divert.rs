
// tl/dr: one input, many prioritized outputs, if top fails, try next until there are none left

use crate::team::Team;
use crate::worker::*;
use crate::tx::Tx;
use crate::hacks::*;

use std::sync::mpsc::*;
use std::sync::mpsc::TrySendError::*;
use std::ops::AddAssign;

pub struct Divert<Message:Send> {
    inner:Team<Vec<Option<Box<Tx<Message=Message>+Send>>>>
}

impl<Message:Send+'static> From<Receiver<Message>> for Divert<Message> {
    fn from(rx:Receiver<Message>) -> Self {
        let f = move |input:Message,_:&mut (),mut shared:&mut Vec<Option<Box<Tx<Message=Message>+Send>>>| {
            let mut x = Some(input); // the object we want to send
            let mut disconnected_channels:usize = 0;
            // Try to send to the first non-blocking channel
            for ref mut o in shared.iter_mut() {
                let mut found_disconnected_channel=false;
                x = match x {
                    Some(y) => match o {
                        Some(t) => match t.try_send(y) {
                            Ok(()) => return Ok(()),
                            Err(Full(z)) => Some(z),
                            Err(Disconnected(z)) => { // trigger the entire cleanup routine
                                found_disconnected_channel = true;
                                Some(z)
                            }
                        },
                        None => Some(y)
                    },
                    None => None
                };
                if found_disconnected_channel {
                    o.take();
                    disconnected_channels += 1;
                }
            }
            for ref mut o in shared.iter_mut() {
                let mut found_disconnected_channel=false;
                x = match x {
                    Some(y) => match o {
                        Some(t) => match t.send(y) {
                            Ok(()) => return Ok(()),
                            Err(e) => {
                                found_disconnected_channel = true;
                                Some(e.0)
                            }
                        },
                        None => Some(y)
                    },
                    None => None
                };
                if found_disconnected_channel {
                    o.take();
                    disconnected_channels += 1;
                }
            }
            remove_none_from_vec(&mut shared,Some(disconnected_channels))?;
            // fail if we still have our object because we could't get rid of it
            match x {
                Some(_) => Err(()),
                None => Ok(())
            }
        };
        let mut t = Team::from(
            Vec::new()
        );
        t += (rx,(),f);
        Self {
            inner:t
        }
    }
}

impl<Message:Send,Output:Tx<Message=Message>+'static+Send> AddAssign<Output>
for Divert<Message> {
    fn add_assign(&mut self,other:Output) {
        self
            .inner
            .shared_state
            .lock()
            .unwrap()
            .push(Some(
                    Box::new(other)
                    as Box<Tx<Message=Message>+Send>)
            )
    }
}

impl<Message:Send+'static> Worker<()> for Divert<Message> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

impl<Message:Send+'static> Wait for Divert<Message> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

#[cfg(test)] use super::drop;
#[cfg(test)]
#[test]
fn test_divert(){
    let (tx1,rx1)=channel();
    let (tx2,rx2)=channel();
    let (tx3,rx3)=channel();
    let (tx4,rx4)=channel();
    
    let mut t = Divert::from(rx1);
    t+=tx2;t+=tx3;t+=tx4;
    
    tx1.send(1).unwrap();
    assert_eq!(rx2.recv().unwrap(),1);
    drop(rx2);
    
    tx1.send(2).unwrap();
    assert_eq!(rx3.recv().unwrap(),2);
    drop(rx3);
    
    tx1.send(3).unwrap();
    assert_eq!(rx4.recv().unwrap(),3);
    
    drop(t);
    drop(tx1);
    
    assert_eq!(rx4.try_recv().is_err(),true);
}
