// tl/dr: many diverse inputs, sequential handling, shared data

use crate::worker::*;
use std::sync::{Arc,Mutex};
use std::sync::mpsc::*;
use std::ops::AddAssign;

pub struct Team<SharedState> {
    pub shared_state:Arc<Mutex<SharedState>>, // only 0 or 1 active worker per team at a time
    pub workers:Vec<Box<Worker<SharedState>>>
}

impl<SharedState> Team<SharedState> {
    fn wait(&mut self) -> bool {
        let mut out=true;
        while let Some(mut x) = self.workers.pop() {
            out&=x.wait();
        }
        out
    }
}

impl<SharedState:Send+'static> From<SharedState> for Team<SharedState> {
    fn from(initial_state:SharedState) -> Self {
        Self {
            shared_state:Arc::new(Mutex::new(initial_state)),
            workers:Vec::new()
        }
    }
}

impl<
    Message:Send+'static,
    PrivateState:Send+'static,
    SharedState:Send+'static,
    Routine:Send+'static+Fn(Message,&mut PrivateState,&mut SharedState)->Result<(),()>
> AddAssign<(Receiver<Message>,PrivateState,Routine)> for Team<SharedState> {
    fn add_assign(&mut self,(rx,initial_state,routine):(Receiver<Message>,PrivateState,Routine)) {
        self.workers.push(
            Box::new(
                TeamWorker::from(
                    (rx,initial_state,self.shared_state.clone(),routine)
                )
            ) as Box<Worker<SharedState>>
        )
    }
}

impl<SharedState> Worker<()> for Team<SharedState> {
    fn wait(&mut self) -> bool {
        self.wait()
    }
}

impl<SharedState> Wait for Team<SharedState> {
    fn wait(&mut self) -> bool {
        self.wait()
    }
}

#[cfg(test)] use super::drop;
#[cfg(test)]
#[test]
fn test_team(){
    let (tx1,rx1)=channel();
    let (tx2,rx2)=channel();
    let (tx3,rx3)=channel();

    let f = move |input,desk:&mut bool,shared:&mut Sender<String>| {
        shared.send(format!("left: {}",desk)).unwrap();
        *desk = input;
        Ok(())
    };

    let g = move |input,desk:&mut u64,shared:&mut Sender<String>| {
        shared.send(format!("right: {}",desk)).unwrap();
        *desk = input;
        Ok(())
    };
    
    let mut t = Team::from(tx3);
    
    t += (rx1,false,f);
    t += (rx2,0,g);
    
    tx1.send(true).unwrap();
    assert_eq!(&rx3.recv().unwrap(),"left: false");
    tx2.send(23).unwrap();
    assert_eq!(&rx3.recv().unwrap(),"right: 0");
    tx2.send(42).unwrap();
    assert_eq!(&rx3.recv().unwrap(),"right: 23");
    tx1.send(false).unwrap();
    assert_eq!(&rx3.recv().unwrap(),"left: true");
    tx2.send(0).unwrap();
    assert_eq!(&rx3.recv().unwrap(),"right: 42");
    
    // unless you send/recv synchronously,
    // the message order might change
    // because both workers are "racing"

    drop(tx1);
    drop(tx2);
    
    t.wait();
    drop(t);
    
    assert_eq!(rx3.recv().is_err(),true);
}
