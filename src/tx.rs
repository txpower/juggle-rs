use std::sync::mpsc::*;

pub trait Tx {
    type Message:Send;
    fn send(&self,t:Self::Message) -> Result<(), SendError<Self::Message>>;
    // default implementation to be overridden
    fn try_send(&self,t:Self::Message) -> Result<(), TrySendError<Self::Message>> {
        match self.send(t) {
            Ok(()) => Ok(()),
            Err(e) => Err(TrySendError::from(e))
        }
    }
}

impl<Message:Send> Tx for Sender<Message> {
    type Message=Message;
    fn send(&self,t:Self::Message) -> Result<(), SendError<Self::Message>> {
        Sender::send(&self,t)
    }
}

impl<Message:Send> Tx for SyncSender<Message> {
    type Message=Message;
    fn send(&self,t:Self::Message) -> Result<(), SendError<Self::Message>> {
        SyncSender::send(&self,t)
    }
    fn try_send(&self,t:Self::Message) -> Result<(), TrySendError<Self::Message>> {
        SyncSender::try_send(&self,t)
    }
}

pub enum MaybeSyncSender<Message:Send> {
    Async(Sender<Message>),
    Sync(SyncSender<Message>)
}

impl<Message:Send>
From<SyncSender<Message>>
for MaybeSyncSender<Message> {
    fn from(x:SyncSender<Message>) -> Self {
        MaybeSyncSender::Sync(x)
    }
}

impl<Message:Send>
From<Sender<Message>>
for MaybeSyncSender<Message> {
    fn from(x:Sender<Message>) -> Self {
        MaybeSyncSender::Async(x)
    }
}

impl<Message:Send> Tx for MaybeSyncSender<Message> {
    type Message=Message;
    fn send(&self,t:Self::Message) -> Result<(), SendError<Self::Message>> {
        match self {
            MaybeSyncSender::Async(ref x) => Tx::send(x,t),
            MaybeSyncSender::Sync(ref x) => Tx::send(x,t)
        }
    }
    fn try_send(&self,t:Self::Message) -> Result<(), TrySendError<Self::Message>> {
        match self {
            MaybeSyncSender::Async(ref x) => Tx::try_send(x,t),
            MaybeSyncSender::Sync(ref x) => Tx::try_send(x,t)
        }
    }
}

pub fn maybe_sync_channel<Message:Send>(size:Option<usize>) -> (MaybeSyncSender<Message>,Receiver<Message>) {
    match size {
        None => {
            let (tx,rx) = channel();
            (MaybeSyncSender::from(tx),rx)
        },
        Some(n) => {
            let (tx,rx) = sync_channel(n);
            (MaybeSyncSender::from(tx),rx)
        }
    }
}
