
// tl/dr: one input, many load-balanced outputs

use crate::team::Team;
use crate::worker::*;
use crate::tx::Tx;
use std::sync::mpsc::*;
use std::sync::mpsc::TrySendError::*;
use std::ops::AddAssign;

// TODO: optimize
struct InnerRobin<Message:Send> {
    pub left:Vec<Box<Tx<Message=Message>+Send>>,
    pub right:Vec<Box<Tx<Message=Message>+Send>>,
    retry:usize
}

impl<Message:Send> From<Vec<Box<Tx<Message=Message>+Send>>>
for InnerRobin<Message> {
    fn from(x:Vec<Box<Tx<Message=Message>+Send>>) -> Self {
        let l=x.len();
        Self {
            left:Vec::with_capacity(l),
            right:x,
            retry:10
        }
    }
}

pub struct RoundRobin<Message:Send> {
    inner:Team<InnerRobin<Message>>
}

impl<Message:Send+'static> From<Receiver<Message>> for RoundRobin<Message> {
    fn from(rx:Receiver<Message>) -> Self {
        let f = move |input:Message,_:&mut (),shared:&mut InnerRobin<Message>| {
            let mut x=Some(input);
            for _ in 0..shared.retry {
                while let Some(w) = shared.left.pop(){
                    x=match x {
                        Some(y) => match w.try_send(y) {
                            Ok(_) => {
                                shared.right.push(w); // keep that Tx in rotation
                                return Ok(());
                            },
                            Err(Full(z)) => {
                                shared.right.push(w); // keep that Tx in rotation
                                Some(z)
                            },
                            Err(Disconnected(z)) => {
                                // drop any disconnected Tx
                                Some(z)
                            }
                        },
                        None => None
                    };
                }
                while let Some(w) = shared.right.pop() {
                    shared.left.push(w)
                }
            }
            while let Some(w) = shared.left.pop(){
                x=match x {
                    Some(y) => match w.send(y) {
                        Ok(_) => {
                            shared.right.push(w); // keep that Tx in rotation
                            return Ok(());
                        },
                        Err(e) => {
                            // drop any disconnected Tx
                            Some(e.0)
                        }
                    },
                    None => None
                };
            }
            while let Some(w) = shared.right.pop() {
                    shared.left.push(w)
            }
            Ok(())
        };
        let mut t = Team::from(
            InnerRobin::from(
                Vec::new()
            )
        );
        t+=(rx,(),f);
        Self {
            inner:t
        }
    }
}

impl<Message:Send,Output:Tx<Message=Message>+'static+Send> AddAssign<Output>
for RoundRobin<Message> {
    fn add_assign(&mut self,other:Output) {
        self
            .inner
            .shared_state
            .lock()
            .unwrap()
            .right
            .push(
                Box::new(other)
                as Box<Tx<Message=Message>+Send>
            )
    }
}

impl<Message:Send+'static> Worker<()> for RoundRobin<Message> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

impl<Message:Send+'static> Wait for RoundRobin<Message> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

#[cfg(test)] use super::drop;
#[cfg(test)]
#[test]
fn test_robin(){
    let (tx1,rx1)=channel();
    let (tx2,rx2)=channel();
    let (tx3,rx3)=channel();
    let (tx4,rx4)=channel();
    
    let mut t = RoundRobin::from(rx1);
    t+=tx2;t+=tx3;t+=tx4;
    
    for n in 1..23 {
        tx1.send(n).unwrap();
        match n % 3 {
            1 => assert_eq!(rx2.recv().unwrap(),n),
            2 => assert_eq!(rx3.recv().unwrap(),n),
            0 => assert_eq!(rx4.recv().unwrap(),n),
            _ => panic!("CPU, memory or OS is broken!")
        }
    }
    
    drop(t);
    drop(tx1);
}
