// a processing element that can be pipelined, inspired by shell pipelines

use crate::tx::*;
use crate::worker::*;
use std::sync::mpsc::*;

pub struct StatefulFilter<Input:Send,State:Send,Output:Send,TxOutput:Tx<Message=Output>+Send> {
    inner:SoloWorker<Input,(State,Option<TxOutput>)>
}

impl<
    Input:Send+'static,
    State:Send+'static,
    Output:Send+'static,
    TxOutput:Tx<Message=Output>+'static+Send,
    MessageRoutine:Send+'static+Fn(Input,&mut State,&mut TxOutput)->Result<(),()>,
    FinalRoutine:Send+'static+Fn(Result<(),()>,&mut State,&mut TxOutput)
> From<(
    Receiver<Input>,
    State,
    Vec<Output>,
    MessageRoutine,
    FinalRoutine,
    TxOutput
)> for StatefulFilter<Input,State,Output,TxOutput> {
    fn from((
            rx,initial_state,initial_output,handle_message,finalize,tx
        ):(
            Receiver<Input>,
            State,
            Vec<Output>,
            MessageRoutine,
            FinalRoutine,
            TxOutput
        )
    ) -> StatefulFilter<Input,State,Output,TxOutput> {
        let f = move |input:Input,(state,tx):&mut (State,Option<TxOutput>)|-> Result<(),()> {
            match tx {
                Some(ref mut t) => handle_message(input,state,t),
                None => Err(())
            }
        };

        let g = move |final_result:Result<(),()>,(state,tx):&mut(State,Option<TxOutput>)| {
            match tx {
                Some(ref mut t) => {
                    finalize(final_result,state,t);
                },
                None => ()
            }
            tx.take();
        };

        for x in initial_output.into_iter() {
            tx
            .send(x)
            .expect("output channel closed before finishing the preamble");                
        }

        Self{
            inner:SoloWorker::from((rx,(initial_state,Some(tx)),f,g))
        }
    }
}

impl<
    Input:Send+'static,
    State:Send+'static,
    Output:Send+'static,
    TxOutput:Tx<Message=Output>+'static+Send
> Worker<()> for StatefulFilter<Input,State,Output,TxOutput> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

impl<
    Input:Send+'static,
    State:Send+'static,
    Output:Send+'static,
    TxOutput:Tx<Message=Output>+'static+Send
> Wait for StatefulFilter<Input,State,Output,TxOutput> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

#[cfg(test)] use super::drop;
#[cfg(test)]
#[test]
fn test_stateful_filter(){
    let (tx2,rx2) = channel();

    let (tx1,rx1) = channel();

    let f = |input,state:&mut u32,tx:&mut Sender<u32>|{
        let old_state = *state;
        *state ^= input;
        match tx.send(old_state) {
            Ok(()) => Ok(()),
            Err(_) => Err(())
        }
    };

    let g = |result:Result<(),()>,state:&mut u32,tx:&mut Sender<u32>|{
        if result.is_ok() {
            tx.send(*state).unwrap()
        } else {
            println!("state={}",*state)
        }

    };

    let mut d = StatefulFilter::from((rx1,1,vec![0,0,0],f,g,tx2));

    tx1.send(2).unwrap();
    tx1.send(4).unwrap();
    tx1.send(8).unwrap();



    // preamble
    assert_eq!(rx2.recv(),Ok(0));
    assert_eq!(rx2.recv(),Ok(0));
    assert_eq!(rx2.recv(),Ok(0));

    // modified data
    assert_eq!(rx2.recv(),Ok(1));
    assert_eq!(rx2.recv(),Ok(3));
    assert_eq!(rx2.recv(),Ok(7));

    drop(tx1); // deadlock if we don't

    // get the final messages
    assert_eq!(rx2.recv(),Ok(15));

    Wait::wait(&mut d);

}
