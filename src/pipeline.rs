// a helper to construct pipelined StatefulFilters

use crate::tx::*;
use crate::stateful_filter::*;
use std::sync::mpsc::*;
use std::ops::Add;
use crate::worker::Wait;

pub type Empty = ();

impl Wait for Empty {
    fn wait(&mut self) -> bool {
        true
    }
}

impl<A:Wait,B:Wait> Wait for (A,B) {
    fn wait(&mut self) -> bool {
        let a = Wait::wait(&mut self.0);
        let b = Wait::wait(&mut self.1);
        a & b
    }
}

pub struct Pipeline<Input:Send,Output:Send,TxInput:Tx<Message=Input>+Send,Inner> {
    rx:Option<Receiver<Output>>,
    tx:Option<TxInput>,
    inner:Inner
}

impl<Message:Send,T:Tx<Message=Message>+Send>
From<(T,Receiver<Message>)>
for Pipeline<Message,Message,T,Empty> {
    fn from((tx,rx):(T,Receiver<Message>)) -> Self {
        Self {
            rx:Some(rx),
            tx:Some(tx),
            inner:()
        }
    }
}

impl<Input:Send,Output:Send,T:Tx<Message=Input>+Send,U:Wait>
Wait for Pipeline<Input,Output,T,U> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

// TODO: refactor, dedup

impl<
    Input:Send+'static,
    Intermediate:Send+'static,
    TxInput:Tx<Message=Input>+'static+Send,
    State:Send+'static,
    Output:Send+'static,
    MessageRoutine:Send+'static+Fn(Intermediate,&mut State,&mut SyncSender<Output>)->Result<(),()>,
    FinalRoutine:Send+'static+Fn(Result<(),()>,&mut State,&mut SyncSender<Output>),
    Inner
> Add<(
    usize,
    State,
    Vec<Output>,
    MessageRoutine,
    FinalRoutine
)> for Pipeline<Input,Intermediate,TxInput,Inner> {
    type Output=Pipeline<
        Input,
        Output,
        TxInput,
        (
            Inner,
            StatefulFilter<Intermediate,State,Output,SyncSender<Output>>
        )
    >;
    fn add(
        self,(
            tx_size,
            initial_state,
            initial_output,
            message_routine,
            final_routine
        ):(
            usize,
            State,
            Vec<Output>,
            MessageRoutine,
            FinalRoutine
        )
    ) -> Self::Output {
        let (tx,rx)=sync_channel(tx_size);
        Pipeline {
            rx:Some(rx),
            tx:self.tx,
            inner:(
                self.inner,
                StatefulFilter::from((
                    self.rx.unwrap(),
                    initial_state,
                    initial_output,
                    message_routine,
                    final_routine,
                    tx
                ))
            )
        }
    }
}

impl<
    Input:Send+'static,
    Intermediate:Send+'static,
    TxInput:Tx<Message=Input>+'static+Send,
    State:Send+'static,
    Output:Send+'static,
    MessageRoutine:Send+'static+Fn(Intermediate,&mut State,&mut Sender<Output>)->Result<(),()>,
    FinalRoutine:Send+'static+Fn(Result<(),()>,&mut State,&mut Sender<Output>),
    Inner
> Add<(
    State,
    Vec<Output>,
    MessageRoutine,
    FinalRoutine
)> for Pipeline<Input,Intermediate,TxInput,Inner> {
    type Output=Pipeline<
        Input,
        Output,
        TxInput,
        (
            Inner,
            StatefulFilter<Intermediate,State,Output,Sender<Output>>
        )
    >;
    fn add(
        self,(
            initial_state,
            initial_output,
            message_routine,
            final_routine
        ):(
            State,
            Vec<Output>,
            MessageRoutine,
            FinalRoutine
        )
    ) -> Self::Output {
        let (tx,rx)=channel();
        Pipeline {
            rx:Some(rx),
            tx:self.tx,
            inner:(
                self.inner,
                StatefulFilter::from((
                    self.rx.unwrap(),
                    initial_state,
                    initial_output,
                    message_routine,
                    final_routine,
                    tx
                ))
            )
        }
    }
}

impl<
    Input:Send+'static,
    Intermediate:Send+'static,
    TxInput:Tx<Message=Input>+'static+Send,
    State:Send+'static,
    Output:Send+'static,
    MessageRoutine:Send+'static+Fn(Intermediate,&mut State,&mut MaybeSyncSender<Output>)->Result<(),()>,
    FinalRoutine:Send+'static+Fn(Result<(),()>,&mut State,&mut MaybeSyncSender<Output>),
    Inner
> Add<(
    Option<usize>,
    State,
    Vec<Output>,
    MessageRoutine,
    FinalRoutine
)> for Pipeline<Input,Intermediate,TxInput,Inner> {
    type Output=Pipeline<
        Input,
        Output,
        TxInput,
        (
            Inner,
            StatefulFilter<Intermediate,State,Output,MaybeSyncSender<Output>>
        )
    >;
    fn add(
        self,(
            size,
            initial_state,
            initial_output,
            message_routine,
            final_routine
        ):(
            Option<usize>,
            State,
            Vec<Output>,
            MessageRoutine,
            FinalRoutine
        )
    ) -> Self::Output {
        let (tx,rx)=maybe_sync_channel(size);
        Pipeline {
            rx:Some(rx),
            tx:self.tx,
            inner:(
                self.inner,
                StatefulFilter::from((
                    self.rx.unwrap(),
                    initial_state,
                    initial_output,
                    message_routine,
                    final_routine,
                    tx
                ))
            )
        }
    }
}

#[cfg(test)] use super::drop;
#[test]
fn test_pipeline() {
    fn inner_test() -> Option<()> {
        println!("");
        let mut p = Pipeline::from(channel()) + (
            false,
            vec![],
            |_:(),state:&mut bool,tx:&mut Sender<bool>|->Result<(),()>{
                let old_state=*state;
                println!("stage 1: {:?}",&old_state);
                *state ^= true;
                tx.send(old_state).and_then(|_|Ok(())).or_else(|_|Err(()))
            },
            |r:Result<(),()>,state:&mut bool,tx:&mut Sender<bool>|{
                println!("finished stage 1: {:?},{:?}",r,tx.send(*state));
            }
        ) + (
            0,
            vec![],
            |input:bool,state:&mut u64,tx:&mut Sender<u64>|->Result<(),()>{
                let old_state=*state;
                println!("stage 2: {:?},{:?}",&old_state,&input);
                if input {
                    *state += 1;
                }
                tx.send(old_state).and_then(|_|Ok(())).or_else(|_|Err(()))
            },
            |r:Result<(),()>,state:&mut u64,tx:&mut Sender<u64>|{
                println!("finished stage 2: {:?},{:?}",r,tx.send(*state));
            }
        ) + (
            (),
            vec![],
            |input:u64,_:&mut (),tx:&mut Sender<String>|->Result<(),()>{
                println!("stage 3: {:?}",&input);
                tx.send(input.to_string()).and_then(|_|Ok(())).or_else(|_|Err(()))
            },
            |r:Result<(),()>,_:&mut (),_:&mut Sender<String>|{
                println!("finished stage 3: {:?}",r);
            }
        );
        {
            let r=p.rx.as_ref();
            {
                let t=p.tx.as_ref();

                t?.send(()).unwrap();
                assert_eq!(r?.recv().unwrap(),String::from("0"));
                t?.send(()).unwrap();
                assert_eq!(r?.recv().unwrap(),String::from("0"));
                t?.send(()).unwrap();
                assert_eq!(r?.recv().unwrap(),String::from("1"));
                t?.send(()).unwrap();
                assert_eq!(r?.recv().unwrap(),String::from("1"));
                t?.send(()).unwrap();
                assert_eq!(r?.recv().unwrap(),String::from("2"));
                t?.send(()).unwrap();
                assert_eq!(r?.recv().unwrap(),String::from("2"));
            }
            {
                drop(p.tx.take());
            }
            assert_eq!(r?.recv().unwrap(),String::from("3"));
        }
        let rx=p.rx.take().unwrap();
        drop(p); // deadlock without
        assert_eq!(rx.recv().unwrap(),String::from("3"));
        assert_eq!(rx.recv().is_err(),true);
        Some(())
    }
    assert_eq!(inner_test(),Some(()));
}
