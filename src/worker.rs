// tl/dr: one input, single-thread event loop

use std::sync::mpsc::*;
use std::sync::{Arc,Mutex};
use std::thread::{spawn,JoinHandle}; // TODO: more lightweight coop threading

/* similar to an Erlang process
 * while the input channel is open
 */

pub trait Wait {
    fn wait(&mut self) -> bool;
}

pub trait MaybeSharedState<SharedState> {
    fn get_shared_state(&self) -> Option<Arc<Mutex<SharedState>>> {
        None
    }
}

pub trait Worker<SharedState> {
    // fn get_shared_state(&self) -> Option<Arc<Mutex<SharedState>>>;
    fn wait(&mut self) -> bool;
}

struct InnerSoloWorker<Message:Send,PrivateState> {
    rx:Receiver<Message>,
    desk:PrivateState,          /// private data of each thread (put Senders here)
    routine:Box<Fn(Message,&mut PrivateState)->Result<(),()>+Send>, // gets repeated for each incoming Message
    final_routine:Option<Box<Fn(Result<(),()>,&mut PrivateState)+Send>>
}

pub struct SoloWorker<Message:Send,PrivateState> {
    inner:Arc<Mutex<InnerSoloWorker<Message,PrivateState>>>,
    thread:Option<JoinHandle<()>>
}

impl<Message:Send,PrivateState:Send> InnerSoloWorker<Message,PrivateState> {
    fn run(&mut self) {
        while let Ok(x) = self.rx.recv() {
            match (*(self.routine))(x,&mut self.desk) {
                Ok(_) => (),
                Err(_) => match self.final_routine{
                    None => return,
                    Some(ref f) => (*(f))(Err(()),&mut self.desk)
                }
            }
        }
        match self.final_routine{
            None => return,
            Some(ref f) => (*(f))(Ok(()),&mut self.desk)
        }
    }
}

impl<Message:Send,PrivateState:Send> SoloWorker<Message,PrivateState> {
    pub fn set_state(&mut self,new_state:PrivateState) -> Result<(),PrivateState> {
        match self.inner.lock() {
            Ok(mut x) => Ok(x.desk=new_state),
            Err(_) => Err(new_state)
        }
    }
}

impl<Message:Send,PrivateState> Worker<()>
for SoloWorker<Message,PrivateState> {
    fn wait(&mut self) -> bool {
        match self.thread.take() {
            None => false,
            Some(t) => t.join().is_ok()
        }
    }
}

impl<Message:Send,PrivateState> Wait
for SoloWorker<Message,PrivateState> {
    fn wait(&mut self) -> bool {
        match self.thread.take() {
            None => false,
            Some(t) => t.join().is_ok()
        }
    }
}

impl<
    Message:Send+'static,
    PrivateState:Send+'static,
    Routine:Send+'static+Fn(Message,&mut PrivateState)->Result<(),()>
> From<(Receiver<Message>,PrivateState,Routine)> for SoloWorker<Message,PrivateState> {
    fn from(
        (rx,desk,routine):(
            Receiver<Message>,
            PrivateState,
            Routine
        )
    ) -> Self {
        let a = Arc::new(
            Mutex::new(
                InnerSoloWorker {
                    rx:rx, desk:desk,
                    routine:Box::new(routine) as Box<Fn(Message,&mut PrivateState)->Result<(),()>+Send>,
                    final_routine:None
                }
            )
        );
        let b=a.clone();
        Self {
            inner:a,
            thread:Some(
                spawn(
                    move || {
                        let mut c=b.lock().unwrap();
                        c.run()
                    }
                )
            )
        }
    }
}

impl<
    Message:Send+'static,
    PrivateState:Send+'static,
    Routine:Send+'static+Fn(Message,&mut PrivateState)->Result<(),()>,
    FinalRoutine:Send+'static+Fn(Result<(),()>,&mut PrivateState)
> From<(Receiver<Message>,PrivateState,Routine,FinalRoutine)> for SoloWorker<Message,PrivateState> {
    fn from(
        (rx,desk,routine,final_routine):(
            Receiver<Message>,
            PrivateState,
            Routine,
            FinalRoutine
        )
    ) -> Self {
        let a = Arc::new(
            Mutex::new(
                InnerSoloWorker {
                    rx:rx, desk:desk,
                    routine:Box::new(routine) as Box<Fn(Message,&mut PrivateState)->Result<(),()>+Send>,
                    final_routine:Some(Box::new(final_routine) as Box<Fn(Result<(),()>,&mut PrivateState)+Send>),
                }
            )
        );
        let b=a.clone();
        Self {
            inner:a,
            thread:Some(
                spawn(
                    move || {
                        let mut c=b.lock().unwrap();
                        c.run()
                    }
                )
            )
        }
    }
}

struct TeamWorkerState<PrivateState,SharedState> {
    private:PrivateState,
    shared:Arc<Mutex<SharedState>>
}

pub struct TeamWorker<Message:Send,PrivateState,SharedState> {
    pub shared:Arc<Mutex<SharedState>>, /// data shared between workers of the same team (put Senders here)
    inner:SoloWorker<Message,TeamWorkerState<PrivateState,SharedState>>,
}

impl<
    Message:Send+'static,
    PrivateState:Send+'static,
    SharedState:Send+'static,
    Routine:Send+'static+Fn(Message,&mut PrivateState,&mut SharedState)->Result<(),()>
> From<(Receiver<Message>,PrivateState,Arc<Mutex<SharedState>>,Routine)> for TeamWorker<Message,PrivateState,SharedState> {
    fn from(
        (rx,desk,shared,routine):(
            Receiver<Message>,
            PrivateState,
            Arc<Mutex<SharedState>>,
            Routine
        )
    ) -> Self {
        let s = shared.clone();
        let f = move |input:Message,state:&mut TeamWorkerState<PrivateState,SharedState>|{
                match state.shared.lock() {
                    Ok(ref mut x) => routine(input,&mut state.private,x),
                    Err(_) => Err(())
                }
            };
        let a = SoloWorker::from((rx,TeamWorkerState{private:desk,shared:shared},f));
        Self {
            inner:a,
            shared:s
        }
    }
}

impl<
    Message:Send+'static,
    PrivateState:Send+'static,
    SharedState:Send+'static,
    Routine:Send+'static+Fn(Message,&mut PrivateState,&mut SharedState)->Result<(),()>,
    FinalRoutine:Send+'static+Fn(Result<(),()>,&mut PrivateState,&mut SharedState)
> From<(Receiver<Message>,PrivateState,Arc<Mutex<SharedState>>,Routine,FinalRoutine)> for TeamWorker<Message,PrivateState,SharedState> {
    fn from(
        (rx,desk,shared,routine,final_routine):(
            Receiver<Message>,
            PrivateState,
            Arc<Mutex<SharedState>>,
            Routine,
            FinalRoutine
        )
    ) -> Self {
        let s = shared.clone();
        let f = move |input:Message,state:&mut TeamWorkerState<PrivateState,SharedState>|{
                match state.shared.lock() {
                    Ok(ref mut x) => routine(input,&mut state.private,x),
                    Err(_) => Err(())
                }
            };
        let g = move |final_result:Result<(),()>,state:&mut TeamWorkerState<PrivateState,SharedState>|{
                match state.shared.lock() {
                    Ok(ref mut x) => final_routine(final_result,&mut state.private,x),
                    Err(_) => ()
                }
            };
        let a = SoloWorker::from((rx,TeamWorkerState{private:desk,shared:shared},f,g));
        Self {
            inner:a,
            shared:s
        }
    }
}

impl<Message:Send,PrivateState,SharedState> Worker<SharedState>
for TeamWorker<Message,PrivateState,SharedState> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

impl<Message:Send,PrivateState,SharedState> Wait
for TeamWorker<Message,PrivateState,SharedState> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

#[cfg(test)] use super::drop;
#[cfg(test)]
#[test]
fn test_team_worker(){
    let mut d;
    let (tx2,rx2) = channel();

    let (tx1,rx1) = channel();

    let a = Arc::new(
        Mutex::new(
            tx2
        )
    );

    let f = move |input,desk:&mut u64,shared:&mut Sender<u64>|{
        shared.send(*desk).unwrap();
        *desk = input;
        Ok(())
    };

    d = TeamWorker::from((rx1,42,a,f));

    tx1.send(23).unwrap();
    tx1.send(47).unwrap();
    tx1.send(0).unwrap();

    assert_eq!(rx2.recv(),Ok(42));
    assert_eq!(rx2.recv(),Ok(23));
    assert_eq!(rx2.recv(),Ok(47));

    drop(tx1); // deadlock if we don't

    Wait::wait(&mut d);

    assert_eq!(d.inner.inner.lock().unwrap().desk.private,0);

}
