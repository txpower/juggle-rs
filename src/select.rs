// tl/dr: many diverse inputs, sequential handling, limit?

use crate::team::*;
use crate::worker::*;
use std::ops::AddAssign;
use std::sync::mpsc::*;

struct InnerSelect<SharedState:Send> {
    left:Option<u128>,
    shared:SharedState
}

pub struct Select<SharedState:Send> {
    inner:Team<InnerSelect<SharedState>>
}

impl<SharedState:Send+'static> From<SharedState> for Select<SharedState> {
    fn from(initial_state:SharedState) -> Self {
        Self {
            inner:Team::from(
                InnerSelect {
                    left:None,
                    shared:initial_state
                }
            )
        }
    }
}

impl<SharedState:Send+'static> From<(SharedState,u128)> for Select<SharedState> {
    fn from((initial_state,left):(SharedState,u128)) -> Self {
        Self {
            inner:Team::from(
                InnerSelect {
                    left:Some(left),
                    shared:initial_state
                }
            )
        }
    }
}

impl<
    Message:Send+'static,
    PrivateState:Send+'static,
    SharedState:Send+'static,
    Routine:Send+'static+Fn(Message,&mut PrivateState,&mut SharedState)->Result<(),()>
> AddAssign<(Receiver<Message>,PrivateState,Box<Routine>)> for Select<SharedState> {
    fn add_assign(
        &mut self,
        (rx,initial_state,routine):(
            Receiver<Message>,
            PrivateState,
            Box<Routine>
        )
    ) {
        let f = move |input:Message,desk:&mut PrivateState,inner:&mut InnerSelect<SharedState>| {
            match inner.left {
                Some(0) => return Err(()),
                Some(ref mut n) => *n-=1,
                None => ()
            }
            match routine(input,desk,&mut inner.shared) {
                Ok(x) => if inner.left==Some(0) { Err(x) } else { Ok(x) },
                Err(x) => Err(x)
            }
        };
        
        self.inner += (rx,initial_state,f);
    }
}

impl<T:Send+'static> Worker<()> for Select<T> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

impl<T:Send+'static> Wait for Select<T> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

#[cfg(test)] use super::drop;
#[test]
fn test_select() {
    let (tx0,rx0) = channel();
    let (tx1,rx1) = sync_channel(0);
    let (tx2,rx2) = sync_channel(0);

    let mut s = Select::from((tx0,1));

    let f=move|input,desk:&mut u8,out:&mut Sender<(u8,u16)>|{
        match out.send((*desk,input)){
            Ok(_) => Ok(()),
            Err(_) => Err(())
        }
    };

    s += (rx1,1,Box::new(f));
    s += (rx2,2,Box::new(f));

    tx1.send(42).unwrap();
    assert_eq!(rx0.recv().unwrap(),(1,42));
    tx2.send(23).unwrap();
    drop(tx1);
    drop(tx2);
    Wait::wait(&mut s);
    drop(s);
    assert_eq!(rx0.recv().is_err(),true);    
}

