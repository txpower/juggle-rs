// tl/dr: one input, many uniform outputs, message is broadcasted

use crate::team::Team;
use crate::worker::*;
use crate::tx::Tx;
use crate::hacks::*;
use std::sync::mpsc::*;
use std::ops::AddAssign;

pub struct Tee<Message:Send+Clone> {
    pub inner:Team<Vec<Option<Box<Tx<Message=Message>+Send>>>>
}

impl<Message:Send+Clone+'static> From<Receiver<Message>> for Tee<Message> {
    fn from(rx:Receiver<Message>) -> Self {
        let f = move |input:Message,_:&mut (),mut shared:&mut Vec<Option<Box<Tx<Message=Message>+Send>>>| {
            let mut disconnected_channels = 0;
            for ref mut o in shared.iter_mut() {
                let mut found_disconnected_channel = false;
                if let Some(t) = o {
                    found_disconnected_channel = t.send(input.clone()).is_err();
                }
                if found_disconnected_channel {
                    o.take(); // remove any disconnected Tx
                    disconnected_channels += 1;
                }
            }
            if disconnected_channels > 0 {
                remove_none_from_vec(&mut shared,Some(disconnected_channels))?;
            }
            Ok(())
        };
        let mut t = Team::from(Vec::new());
        t += (rx,(),f);
        Self {
            inner:t
        }
    }
}

impl<Message:Send+Clone+'static> Worker<()> for Tee<Message> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

impl<Message:Send+Clone+'static> Wait for Tee<Message> {
    fn wait(&mut self) -> bool {
        Wait::wait(&mut self.inner)
    }
}

impl<Message:Send+Clone,Output:Send+Tx<Message=Message>+'static> AddAssign<Output>
for Tee<Message> {
    fn add_assign(&mut self,other:Output) {
        self
            .inner
            .shared_state
            .lock()
            .unwrap()
            .push(
                Some(
                    Box::new(other)
                    as Box<Tx<Message=Message>+Send>)
            )
    }
}

#[cfg(test)] use super::drop;
#[cfg(test)]
#[test]
fn test_tee(){
    let (tx1,rx1)=channel();
    let (tx2,rx2)=channel();
    let (tx3,rx3)=channel();
    let (tx4,rx4)=channel();
    
    let mut t = Tee::from(rx1);
    t+=tx2;t+=tx3;t+=tx4;
    
    for n in 1..23 {
        tx1.send(n).unwrap();
        assert_eq!(rx2.recv().unwrap(),n);
        assert_eq!(rx3.recv().unwrap(),n);
        assert_eq!(rx4.recv().unwrap(),n);
    }
    
    drop(t);
    drop(tx1);
    
    assert_eq!(rx2.recv().is_err(),true);
    assert_eq!(rx3.recv().is_err(),true);
    assert_eq!(rx4.recv().is_err(),true);

}
