/* The crate name taken literally:
 *
 * A really silly example to demo message passing between actor threads
 * imitating the "3-ball cascade" juggling pattern.
 * While the timing of both "hands" is unpredictable (because of
 * multithreading), every "hand" (thread) goes always through the same
 * sequence---CR TR CG TG CB TB (C=catch, T=throw, R=red, G=green, B=blue)---
 * and every "ball" (message) is going through the same sequence---
 * RC, RT, LC, LT (R=right hand, L=left hand).
 *
 * So while the overall sequence of events is nondeterministic,
 * it is deterministic for each ball and each hand.
 *
 * cargo run --example juggle | grep left
 * cargo run --example juggle | grep right
 * cargo run --example juggle | grep red
 * cargo run --example juggle | grep green
 * cargo run --example juggle | grep blue
 *
 * So while a "natural" event sequence would look like
 *   rtr ltb rcg lcr rtg ltr rcb lcg rtb ltg rcr lcb
 * you can also have
 *   rcr rtr rcg rtg rcb rtb lcr ltr lcg ltg lcb ltb
 */

// LICENSE: GNU GPLv2

extern crate colorful;
extern crate juggle;

use juggle::*;
use std::sync::mpsc::*;
use std::thread::*;
use std::time::Duration;
use colorful::core::color_string::CString;
use colorful::Colorful;

fn new_hand(name:&str,rx:Receiver<CString>,tx:Sender<CString>)
-> SoloWorker<CString,(String,Sender<CString>)> {

    let f = move |input,(name,tx):&mut (String,Sender<CString>)|{
        println!("{} hand\tcatches\t{}",&name,&input);

        {
            // change or delete and see what happens
            yield_now(); // request switching threads
            sleep(Duration::from_millis(1000));
        }

        println!("{} hand\tthrows\t{}",&name,&input);
        if tx.send(input).is_err() {
            println!("oops ({})",&name);
            return Err(());
        }

        {
            // change or delete and see what happens
            sleep(Duration::from_millis(1000));
            yield_now(); // request switching threads
        }

        Ok(())
    };

    SoloWorker::from((rx,(String::from(name),tx),f))
}

fn main() {
    let (left_tx,left_rx) = channel::<CString>();
    let (right_tx,right_rx) = channel::<CString>();

    let initial_tx = right_tx.clone();

    // left hand throws right
    let mut lh = new_hand("left",left_rx,right_tx);

    // right hand throws left
    let mut rh = new_hand("right",right_rx,left_tx);

    for x in vec![
        String::from("red ball").light_red(),
        String::from("green ball").light_green(),
        String::from("blue ball").light_blue(),
    ] {
        initial_tx.send(x).unwrap();
    }

    Wait::wait(&mut lh);
    Wait::wait(&mut rh);
}
